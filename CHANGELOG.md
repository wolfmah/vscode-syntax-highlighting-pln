# Change Log

All notable changes to the `pf-syntax` extension will be documented in this file.

## [Unreleased]
### Added
### Changed
### Removed
### Fix

## [0.1.0] - 2023-09-14
### Added
- Initial release
